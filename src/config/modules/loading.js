import jquery from 'jquery'
export default {
    state: {
        loading: false
    },
    getters: {
        getLoading: (state) => state.loading
    },
    mutations: {
        setLoading: (state, loading) => {
            state.loading = loading;
        }
    },
    actions: {
        setLoading: ({ commit }, payload) => {
            commit('setLoading', payload)
        }
    }
}