import jquery from 'jquery'
export default {
    state: {
        errorModal: {
            show: false,
            errors: []
        }
    },
    getters: {
        getErrorModal: (state) => state.errorModal
    },
    mutations: {
        setErrorModal: (state, errorModal) => {
            state.errorModal = errorModal;
        }
    },
    actions: {
        setErrorModal: ({ commit }, payload) => {
            commit('setErrorModal', payload)
        }
    }
}