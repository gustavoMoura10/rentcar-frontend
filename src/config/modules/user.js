export default {
    state: {
        user: null
    },
    getters: {
        getUser: state => state.user
    },
    mutations: {
        setUser: (state, user) => {
            state.user = user
        }
    },
    actions: {
        setUser: ({ commit }, payload) => {
            commit('setUser', payload);
        }
    }
}