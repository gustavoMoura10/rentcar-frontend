import Axios from "axios";
const baseURL = "http://localhost:3030";
const AxiosHelper = Axios.create({
  baseURL,
});
AxiosHelper.interceptors.request.use(function (config) {
  if (localStorage.getItem('token')) {
    const token = localStorage.getItem('token');
    config.headers.Authorization = `Bearer ${token}`
  }
  return config;
}, function (error) {
  const status = error?.response?.status;
  if (status && (status === 401 || status === 403)) {
    localStorage.clear()
  }
  return Promise.reject(error);
});

export default AxiosHelper;
