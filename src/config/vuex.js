import {
    createStore
} from 'vuex';
import user from './modules/user'
import loading from './modules/loading'
import errorModal from './modules/errorModal'
export default createStore({
    modules: {
        user,
        loading,
        errorModal
    }
})