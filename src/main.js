import {
  createApp
} from "vue";
import App from "./App.vue";
import initialRoutes from "./routes/initialRoutes";
import "bootstrap/dist/css/bootstrap.min.css";
import * as jquery from "jquery";
import "popper.js";
import "bootstrap/dist/js/bootstrap.js";
import "@fortawesome/fontawesome-free/css/all.css";
import AxiosHelper from "./config/AxiosHelper";
import vuex from "./config/vuex";
const app = createApp(App)
app.use(initialRoutes)
app.use(vuex)
app.config.globalProperties.$http = AxiosHelper
app.config.globalProperties.$jquery = jquery
app.mount("#app");