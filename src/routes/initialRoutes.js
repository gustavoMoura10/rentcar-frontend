import { createWebHistory, createRouter } from "vue-router";
import Home from "../views/Home.vue";
import Register from "../views/Register.vue";
import Login from "../views/Login.vue";
import EditInfo from "../views/EditInfo.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,

  },
  {
    path: "/editInfo",
    name: "EditInfo",
    component: EditInfo,
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem('token') && localStorage.getItem('user')) {
        next()
      } else {
        next('/')
      }
    }
  },
];

const initialRoutes = createRouter({
  history: createWebHistory(),
  routes,
});

export default initialRoutes;
